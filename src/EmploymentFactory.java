import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class EmploymentFactory {

	public Employment hire(Employee e, Company c, String position, int salary, Date date){
		Employment emp = new Employment(position, salary, date);
		emp.setCompanies(c);
		emp.setEmployees(e);
		if(emp.getEndDate()== null){
			c.addEmployment(emp);
			e.addEmployment(emp);
		}
		else if(emp.getEndDate()!= null){
			c.removeEmployment(emp);
			e.removeEmployment(emp);
		}
		return emp;
	}

	public List<Employment> fire(Employee e, Company c, List<Employment> empList){
		for(Employment empl: empList){
			if(empl.getEndDate()!=null){
				c.removeEmployment(empl);
				e.removeEmployment(empl);
			}
		}
		empList.removeIf(a->a.getEndDate()!=null);
		return empList;
	}


	public List<Employee> comapanyEmployees(Company company, List<Employment> emp) {
		for(Employment e: emp){
			if(e.getEndDate()!=null){
				emp.remove(e);
				company.removeEmployment(e);
			}
		}
		return emp.stream().filter(u -> u.getCompanies() == company)
				.map(Employment::getEmployees).collect(Collectors.toList());
	}

	public List<Company> whereEmployeeWorks(Employee empl, List<Employment> emp) {
		return emp.stream().filter(e -> e.getEmployees() == empl)
				.map(Employment::getCompanies).collect(Collectors.toList());
	}
}
